import * as React from "react";
import styled from "styled-components";
import { BottomNavigationBar } from "../components/BottomNavigationBar";
import { useNavigate } from "react-router-dom";
import { useState, useEffect, createContext } from "react";
import axios from "axios";
import { BACKEND_ENDPOINT } from "../api/api";
import { FaStar } from "react-icons/fa";
import { useAuth } from "../contexts/AuthProvider";

export const DataContext = createContext(0);

const formatDatestamp = (dateString) => {
  const notificationDate = new Date(dateString);
  const today = new Date();

  if (notificationDate.toDateString() === today.toDateString()) {
    return "Today";
  } else {
    const options = { day: "numeric", month: "long", year: "numeric" };
    const formattedDate = notificationDate.toLocaleDateString("en-US", options);
    return formattedDate;
  }
};

const formatKaryakramTitle = (title) => {
  return title
    .split("_")
    .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
    .join(" ");
};

function Review() {
  const [reviews, setreviews] = useState([]);
  const navigate = useNavigate();
  const { user: authUser } = useAuth();
  const [loading, setLoading] = useState(false);
  const [showForm, setShowForm] = useState(true);
  const [formData, setFormData] = useState({
    shibir_id: authUser?.id,
    other: "",
  });
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);

  const fetchData = async () => {
    try {
      let response = await axios.get(`${BACKEND_ENDPOINT}/review/get_all`);
      setreviews(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchData();

    const interval = setInterval(() => {
      fetchData();
    }, 30000);

    return () => clearInterval(interval);
  }, []);

  const handleRatingChange = (karyakram, rating) => {
    setFormData((prevFormData) => ({
      ...prevFormData,
      [karyakram]: rating,
    }));
  };

  const updateUserInLocalStorage = (user) => {
    localStorage.setItem("user", JSON.stringify(user));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    const { other, ...ratings } = formData;
    const hasRatings = Object.values(ratings).every(
      (rating) => rating !== undefined && rating !== ""
    );

    if (!hasRatings) {
      alert("Please provide ratings for all karyakram.");
      setLoading(false);
      return;
    }

    try {
      await axios.post(`${BACKEND_ENDPOINT}/review/post_review`, formData);
      const updatedUser = { ...authUser, is_reviewed: "yes" };
      updateUserInLocalStorage(updatedUser);
      navigate("/home");
    } catch (error) {
      console.error("There was an error submitting the reviews!", error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    const storedUser = JSON.parse(localStorage.getItem("user"));
    if (storedUser?.is_reviewed === "yes") {
      setShowForm(false);
    }
  }, []);

  useEffect(() => {
    const { other, ...ratings } = formData;
    const allRatingsFilled = Object.values(ratings).every(
      (rating) => rating !== undefined && rating !== ""
    );
    setIsButtonDisabled(!allRatingsFilled);
  }, [formData]);

  const fetchKaryakram = [
    { id: "1", karyrakram: "pravchan_gnannayan_swami" },
    { id: "2", karyrakram: "workshop_gnannayan_swami" },
    { id: "3", karyrakram: "pravchan_gnanvatsal_swami" },
    { id: "4", karyrakram: "pravchan_ravjibhai_vaishnav" },
    { id: "5", karyrakram: "pravachan_harinarayan_swami" },
    { id: "6", karyrakram: "samvad" },
    { id: "7", karyrakram: "audio_video" },
    { id: "9", karyrakram: "sangit" },
  ];

  return (
    <Container>
      <BackButton
        onClick={() => navigate("/home")}
        src="https://cdn.builder.io/api/v1/image/assets/TEMP/a9737027d53672d51e861c036db609e65e7478afbce397041e33ffa50b82a036?apiKey=3250d16d0ad044539de68d3e33600ce8&"
        alt="Back icon"
      />
      <Header>
        <Profile>
          <ProfileImage
            src="https://cdn.builder.io/api/v1/image/assets/TEMP/ab67d02a8b8ad216eae4c696c2a0e629a71d161cc2bc274895d9505e7b4ec172?apiKey=3250d16d0ad044539de68d3e33600ce8&"
            alt="Profile"
          />
          <ProfileName>Reviews</ProfileName>
        </Profile>
      </Header>
      <Content>
        {showForm ? (
          <ReviewForm onSubmit={handleSubmit}>
            <FormTitle>Submit Your Review</FormTitle>

            {fetchKaryakram.map((option) => (
              <KaryakramGroup key={option.id}>
                <KaryakramTitle>
                  {formatKaryakramTitle(option.karyrakram)}
                </KaryakramTitle>
                <FormGroup>
                  <Stars>
                    {["મધ્યમ", "સારું", "ઉત્તમ"].map((label) => (
                      <StarContainer key={label}>
                        <Star
                          selected={formData[option.karyrakram] === label}
                          onClick={() =>
                            handleRatingChange(option.karyrakram, label)
                          }
                        >
                          <FaStar />
                        </Star>
                        <StarLabel>{label}</StarLabel>
                      </StarContainer>
                    ))}
                  </Stars>
                </FormGroup>
              </KaryakramGroup>
            ))}
            <FormGroup>
              <Label htmlFor="other">Your review</Label>
              <Input
                type="text"
                id="other"
                name="other"
                value={formData.other}
                onChange={(e) =>
                  setFormData({ ...formData, other: e.target.value })
                }
                placeholder="Enter your review (Optional)"
              />
            </FormGroup>
            <SubmitButton type="submit" disabled={isButtonDisabled || loading}>
              {loading ? "Submitting..." : "Submit All Reviews"}
            </SubmitButton>
          </ReviewForm>
        ) : (
          <>
            <Message>You have already submitted your review.</Message>
            <Contents>
              {Array.isArray(reviews) &&
                reviews.map((item, index) => (
                  <Card key={index}>
                    <Description>{item.other}</Description>
                    <Author>
                      By : {item.name} ({item.mandal})
                    </Author>
                  </Card>
                ))}
            </Contents>
          </>
        )}
      </Content>
      <BottomNavigationBar />
    </Container>
  );
}

const DateTime = styled.div`
  color: rgba(39, 0, 37, 0.5);
  font-weight: bold;
  font-size: 1rem;
`;
const Description = styled.p`
  margin-top: 16px;
  overflow: hidden;
  color: rgba(39, 0, 37, 0.5);
  text-align: justify;
  font-weight: bold;
  font-size: 1rem;
`;

const Author = styled.div`
  color: black;
  margin-top: 8px;
`;
const Card = styled.article`
  border-radius: 16px;
  border: 0px solid #1d0f2a;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  background: linear-gradient(168deg, #fff 0%, #e2c2ff 70.31%);
  margin-top: 10px;
  padding: 24px;
`;

const Container = styled.div`
  min-height: 100vh;
  background: linear-gradient(180deg, #ffffff 0%, #e2c2ff 100%);
  display: flex;
  flex-direction: column;
  position: relative;
`;

const BackButton = styled.img`
  width: 13px;
  aspect-ratio: 0.65;
  object-fit: auto;
  position: absolute;
  top: 20px;
  left: 20px;
`;

const Header = styled.header`
  display: flex;
  align-items: center;
  padding: 24px;
  margin-top: 2rem;
`;

const Profile = styled.div`
  display: flex;
  align-items: center;
`;

const ProfileImage = styled.img`
  width: 25px;
  aspect-ratio: 0.96;
  object-fit: auto;
  margin-right: 8px;
`;

const ProfileName = styled.div`
  font-weight: bold;
  font-size: 20px;
`;

const AddButton = styled.img`
  width: 26px;
  aspect-ratio: 1;
  object-fit: auto;
  margin-left: 8px;
`;

const Content = styled.main`
  flex: 1;
  padding: 0 24px;
`;

const KaryakramGroup = styled.div`
  margin-bottom: 2rem;
`;

const KaryakramTitle = styled.h3`
  margin-bottom: 1rem;
  font-weight: bold;
  text-transform: capitalize;
`;

const FormGroup = styled.div`
  margin-bottom: 1rem;
`;

const Stars = styled.div`
  display: flex;
  gap: 1rem;
`;

const StarContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Star = styled.div`
  cursor: pointer;
  color: ${(props) => (props.selected ? "#ffc107" : "#e4e5e9")};
`;

const StarLabel = styled.span`
  margin-top: 0.25rem;
  font-size: 0.75rem;
  color: #000;
`;

const Contents = styled.main`
  flex: 1;
  padding: 0 24px;
  overflow-y: auto;
`;

const SubmitButton = styled.button`
  width: 100%;
  padding: 0.75rem;
  border: none;
  border-radius: 4px;
  background: #6200ea;
  color: #fff;
  font-size: 1rem;
  font-weight: bold;
  cursor: pointer;
  margin-bottom: 25px;
  &:hover {
    background: #3700b3;
  }
  &:disabled {
    background: #b0a1f2;
    cursor: not-allowed;
  }
`;

const ReviewForm = styled.form`
  width: 100%;
  max-width: 600px;
  background: linear-gradient(168deg, #ffff 0%, #e2c2ff 0%);
  padding: 2rem;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
`;

const FormTitle = styled.h2`
  text-align: center;
  margin-bottom: 1.5rem;
`;

const Label = styled.label`
  display: block;
  margin-bottom: 0.5rem;
  font-weight: bold;
`;

const Input = styled.input`
  width: 100%;
  padding: 0.5rem;
  border: 1px solid #ddd;
  border-radius: 4px;
`;

const Message = styled.div`
  text-align: center;
  margin-top: 2rem;
  font-size: 1.2rem;
  color: #555;
`;

export default Review;
