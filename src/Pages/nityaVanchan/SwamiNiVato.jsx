import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';
import { BACKEND_ENDPOINT } from '../../api/api';
import axios from 'axios';


const SwamiNiVato = () => {
    const [swamiVato, setSwamiVato] = useState();
    const navigate = useNavigate();

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get(`${BACKEND_ENDPOINT}/nitya_vanchan/get`)
                setSwamiVato(response.data)
            } catch (error) {
                console.error(error);
            }
        }

        fetchData();
    }, []);

    console.log(swamiVato);

    return (
        <Background>
            <BackButton onClick={() => navigate('/home')}
                src="https://cdn.builder.io/api/v1/image/assets/TEMP/a9737027d53672d51e861c036db609e65e7478afbce397041e33ffa50b82a036?apiKey=3250d16d0ad044539de68d3e33600ce8&"
                alt="Back icon"
            />

            <Content>
                <Profile>
                    <ProfileName>Swami ni Vato</ProfileName>
                </Profile>

                <Paragraph>
                    {Array.isArray(swamiVato) && (<>
                        <Paragraph>--{swamiVato[0].swamini_vaat_1}</Paragraph>
                        <br />
                        <Paragraph>--{swamiVato[0].swamini_vaat_2}</Paragraph>
                        <br />
                        <Paragraph>--{swamiVato[0].swamini_vaat_3}</Paragraph>
                        <br />
                        <Paragraph>--{swamiVato[0].swamini_vaat_4}</Paragraph>
                        <br />
                        <Paragraph>--{swamiVato[0].swamini_vaat_5}</Paragraph>
                        <br />
                        <Paragraph>--{swamiVato[0].swamini_vaat_6}</Paragraph>
                        <br />
                        <Paragraph>--{swamiVato[0].swamini_vaat_7}</Paragraph>
                        <br />
                        <Paragraph>--{swamiVato[0].swamini_vaat_8}</Paragraph>
                        <br />
                        <Paragraph>--{swamiVato[0].swamini_vaat_9}</Paragraph>
                        <br />
                        <Paragraph>--{swamiVato[0].swamini_vaat_10}</Paragraph>
                    </>
                    )}
                </Paragraph>
            </Content>
        </Background>
    );
}

const Background = styled.div`
    width: 100%;
    min-height: 100vh;
    background: var(--Light-Pink-Gradient, linear-gradient(168deg, #FFF 0%, #E2C2FF 70.31%));  
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 2rem;
    box-sizing: border-box;
`;

const BackButton = styled.img`
width: 13px;
aspect-ratio: 0.65;
object-fit: auto;
position: absolute;
top: 20px;
left: 20px;
`;

const Content = styled.div`
    width: 100%;
    max-width: 600px;
    background: white;
    border-radius: 10px;
    padding: 2rem;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
    display: flex;
    flex-direction: column;
    align-items: center;
`;

const Profile = styled.div`
    text-align: center;
    margin-bottom: 1.5rem;
`;

const ProfileName = styled.div`
    font-weight: bold;
    font-size: 24px;
    color: #4A4A4A;
`;

const Paragraph = styled.div`
    font-size: 16px;
    color: #4A4A4A;
    line-height: 1.5;
    text-align: justify;
`;

export default SwamiNiVato