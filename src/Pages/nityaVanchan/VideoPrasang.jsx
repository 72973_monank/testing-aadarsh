import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import { BACKEND_ENDPOINT } from "../../api/api";
import axios from "axios";

const VideoPrasang = () => {
  const [prasang, setPrasang] = useState();
  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `${BACKEND_ENDPOINT}/nitya_vanchan/get`
        );
        setPrasang(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  }, []);
  console.log(prasang);

  return (
    <Background>
      <BackButton
        onClick={() => navigate("/home")}
        src="https://cdn.builder.io/api/v1/image/assets/TEMP/a9737027d53672d51e861c036db609e65e7478afbce397041e33ffa50b82a036?apiKey=3250d16d0ad044539de68d3e33600ce8&"
        alt="Back icon"
      />

      <Content>
        {/* <Profile>
                    <ProfileName>Video Prasang</ProfileName>
                </Profile> */}

        {Array.isArray(prasang) && (
          <>
            {/* <a href={prasang[0].psm_prasang}><h1>Pramukh Swami Prasang video Link</h1></a>
                    <br />
                    <br />
                    <a href={prasang[0].msm_prasang}><h1>Mahant Swami Prasang video Link</h1></a> */}
            <h1>Pramukh Prasangam</h1>
            <p>
              <p>બ્રહ્મસ્વરૂપ પ્રમુખસ્વામી મહારાજ, </p>સન ૧૯૮૦માં મોતિયો
              ઉતરાવ્યા બાદ સ્વામીશ્રીનાં ચશ્માંનાં નંબર બદલાયા હતા. તેથી નવા કાચ
              નંખાવવા માટે ચશ્માં એક હરિભક્તને આપવામાં આવ્યા. તેઓએ ચશ્માનાં કાચ
              તો બદલાવ્યા પણ સાથે સાથે સ્વામીશ્રીનાં ચશ્માંની સસ્તી અને સાદી
              ફ્રેમને પણ બદલી નાંખી. નવી, સુંદર અને કીમતી ફ્રેમમાં નવા નંબર
              મુજબના કાચ જડી તેઓ સ્વામીશ્રી પાસે ચશ્માં લઈ આવ્યા. તે જોઈ
              સ્વામીશ્રીએ તરત જ પૂછ્યું: 'આ નવી ફ્રેમ કેમ કરાવી?' 'બાપા! આ સારી
              લાગશે.’ પણ સ્વામીશ્રીએ જૂની ફ્રેમ જ રાખવાનો આગ્રહ સેવ્યો. સામે તે
              હરિભક્ત પણ નવી, આકર્ષક ફ્રેમવાળાં ચશ્માં જ સ્વામીશ્રી પહેરે તે
              માટે પ્રેમસભર દલીલ કરતા રહ્યા. ત્યારે સ્વામીશ્રીએ તેઓને પૂછ્યું :
              ‘મારે જોવાનું છે તે કાચમાંથી છે કે ફ્રેમમાંથી ?' આ સાંભળતાં તે
              હરિભક્ત નિરુત્તર થઈ ગયા. સ્વામીશ્રીએ જૂની, સસ્તી અને સાદી ફ્રેમથી
              જ કામ ચલાવ્યું.
            </p>
            <a href="https://youtu.be/zmBJSX9MBKs?feature=shared">
              <h2>Pramukh Chhabi Video Prasang</h2>
            </a>
          </>
        )}
      </Content>
      <Content style={{ marginTop: "2rem" }}>
        <h1>Mahant Prasangam</h1>
        <p>
          પ્રશ્ન : અમારે પણ વિકટ પરિસ્થિતિઓનો સામનો કરવા શું કરવું જોઈએ ? તેનું
          માર્ગદર્શન આપો.
        </p>{" "}
        <p>
          મહંત સ્વામી મહારાજ : 'યોગીબાપાએ કહ્યું છે : સહન કરે તો મોટાપુરુષ
          જીવમાંથી રાજી થાય. આવી વાત શાસ્ત્રોમાંય લખી નથી. પ્રમુખ સ્વામી મહારાજે
          ઘણું સહન કર્યું છે. પહેલાં તો આપણે જયાં-ત્યાં, જેવું-તેવું, જેમ-તેમ
          ચલાવી લઈએ તો સુખ થાય. આ જોઈએ ને તે જોઈએ - એવું કહીએ તો તેનો અંત જ ન
          આવે. યોગીબાપા અને પ્રમુખ સ્વામી મહારાજે ઘણી શિખામણો આપી છે, તે પ્રમાણે
          રહીએ તો સુખ, નહી તો અશાંતિ. સહન કરવું એમાં બહુ માલ છે. થોડું અઘરું છે,
          પણ લાઈનમાં ચઢી જઈએ તો ટેવાઈ જઈએ. અને તો કોઈ પ્રશ્ન જ ન રહે.”
        </p>
        <p>
          પ્રશ્ન : 'અમે નિયમો તો પાળીએ છીએ, પણ કોઈ વખત નિયમ લોપ થઈ જાય. છે, કારણ
          કે દઢતા નથી. તો આપ શી રીતે દરેક આજ્ઞા દૃઢતાથી પાળી શક્યા ?
        </p>
        <p>
          સ્વામીશ્રી : 'ભગવાન છે તેવી દૃઢ નિષ્ઠા અને વિષય તુચ્છ છે. તેવું
          યોગીબાપાએ દૃઢ કરાવ્યું છે. આપણા વિચારોને દિશા આપી છે. માટે આ એક વસ્તુ
          સાચી છે, તે દૃઢ કરવું. સ્ટાર્ટિંગ ગમે ત્યાંથી થાય, પણ પહોંચવાનું છે
          શ્રીજીમહારાજ પાસે. ભગવાન ને સંત સાચા છે, તેમની દરેક આજ્ઞા આપણા કલ્યાણ
          માટે છે તેવું દૃઢપણે રહેવું જોઈએ. આપણે ગમે તેવા હોઈએ, આ નિષ્ઠા દૃઢ
          રાખવી અને નિયમ રાખવા, તો બીજું તો થઈ રહેશે. નિયમ પાળવામાં પ્રયત્ન કરવો
          પડે અને ધીરજ તથા વિશ્વાસ રાખવાં પડે, ઉતાવળે કામ ન થાય. આટલા વિચાર હશે
          તો બધું થઈ રહેશે. વિશ્વાસ અને દૃઢ શ્રદ્ધા રાખવી કે કામ થશે.
        </p>
        <a href="https://youtu.be/Va6cDKbdvd0?feature=shared">
          <h2>Mahant Prasangam</h2>
        </a>
      </Content>
    </Background>
  );
};

const Background = styled.div`
  width: 100%;
  min-height: 100vh;
  background: var(
    --Light-Pink-Gradient,
    linear-gradient(168deg, #fff 0%, #e2c2ff 70.31%)
  );
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 2rem;
  box-sizing: border-box;
`;

const BackButton = styled.img`
  width: 13px;
  aspect-ratio: 0.65;
  object-fit: auto;
  position: absolute;
  top: 20px;
  left: 20px;
`;

const Content = styled.div`
  width: 100%;
  max-width: 600px;
  background: white;
  border-radius: 10px;
  padding: 2rem;
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Profile = styled.div`
  text-align: center;
  margin-bottom: 1.5rem;
`;

const ProfileName = styled.div`
  font-weight: bold;
  font-size: 24px;
  color: #4a4a4a;
`;

const Paragraph = styled.div`
  font-size: 16px;
  color: #4a4a4a;
  line-height: 1.5;
  text-align: justify;
`;

export default VideoPrasang;
