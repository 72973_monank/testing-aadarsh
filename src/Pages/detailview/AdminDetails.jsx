import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { BACKEND_ENDPOINT } from '../../api/api';

const AdminDetails = () => {
    const [details, setDetails] = useState([]);
    const [singleData, setSingleData] = useState(null);
    const [detailedView, setDetailedView] = useState(false);
    const [selectedBus, setSelectedBus] = useState('');
    const [searchTerm, setSearchTerm] = useState('');
    const navigate = useNavigate();

    useEffect(() => {
        const fetchDetails = async () => {
            const user = JSON.parse(localStorage.getItem('user'));
            console.log(user.id);
            try {
                const response = await axios.post(`${BACKEND_ENDPOINT}/user/get_all_user`, {
                    shibir_id: user.id
                });
                setDetails(response.data);
            } catch (error) {
                console.error(error);
            }
        }

        fetchDetails();
    }, []);

    const handleBusChange = (e) => {
        setSelectedBus(e.target.value);
    };

    const handleSearchChange = (e) => {
        setSearchTerm(e.target.value);
    };

    const handleClick = (yuvak) => {
        setSingleData(yuvak);
        setDetailedView(true);
    };

    const handleCloseDetailedView = () => {
        setDetailedView(false);
        setSingleData(null);
    };

    const filteredDetails = details.filter((yuvak) => {
        const matchesBus = selectedBus ?
            (selectedBus === 'karyakar' ? yuvak.permission.role !== 'yuvak' || yuvak.permission.role === 'admin' : yuvak.busNumber === selectedBus)
            : true;
        const matchesName = yuvak.name.toLowerCase().includes(searchTerm.toLowerCase());
        // const matchesLastName = yuvak.last_name.toLowerCase().includes(searchTerm.toLowerCase());
        return matchesBus && matchesName;
    });



    return (
        <Background>
            <BackIcon onClick={() => navigate('/user-profile')}
                src="https://cdn.builder.io/api/v1/image/assets/TEMP/a9737027d53672d51e861c036db609e65e7478afbce397041e33ffa50b82a036?apiKey=3250d16d0ad044539de68d3e33600ce8&"
                alt="Back icon"
            />

            <input
                type='text'
                placeholder='Search name here'
                value={searchTerm}
                onChange={handleSearchChange}
                style={{ background: 'transparent', margin: '10px' }}
            />
            <select onChange={handleBusChange} style={{ background: 'transparent', margin: '10px' }}>
                <option value=''>Select bus for filter</option>
                <option value=''>view all</option>
                <option value='1'>Bus-1</option>
                <option value='2'>Bus-2</option>
                <option value='3'>Bus-3</option>
                <option value='4'>Bus-4</option>
                <option value='5'>Bus-5</option>
                <option value='6'>Bus-6</option>
                <option value='7'>Bus-7</option>
                <option value='8'>Bus-8</option>
                <option value='9'>Bus-9</option>
                <option value='10'>Bus-10</option>
                <option value='11'>Bus-11</option>
                <option value='12'>Bus-12</option>
                <option value='13'>Bus-13</option>
                <option value='14'>Bus-14</option>
                <option value='15'>Bus-15</option>
                <option value='karyakar'>Karyakars</option>
            </select>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Mandal</th>
                    </tr>
                </thead>
                <tbody>
                    {Array.isArray(filteredDetails) && filteredDetails.map((yuvak, index) => (
                        <tr key={index}>
                            <td>{yuvak.name}</td>
                            <td>{yuvak.mandal}</td>
                            <td>
                                <Details onClick={() => handleClick(yuvak)}>View Details</Details>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>

            {detailedView && (
                <>
                    <TransparentBackground onClick={handleCloseDetailedView} />
                    <DetailedViewModal>
                        <CloseButton onClick={handleCloseDetailedView}>X</CloseButton>
                        {singleData && (
                            <DetailedViewData>
                                <p style={{ margin: '0.3rem' }}><strong>Name:</strong> {singleData.name}</p>
                                <p style={{ margin: '0.3rem' }}><strong>Shibir Id:</strong> {singleData.shibir_id}</p>
                                <p style={{ margin: '0.3rem' }}><strong>Xetra:</strong> {singleData.xetra}</p>
                                <p style={{ margin: '0.3rem' }}><strong>Mandal:</strong> {singleData.mandal}</p>
                                <p style={{ margin: '0.3rem' }}><strong>Phone no:</strong> {singleData.phone_number}</p>
                                <p style={{ margin: '0.3rem' }}><strong>Emergency no:</strong> {singleData.emergency_number}</p>
                                <p style={{ margin: '0.3rem' }}><strong>Bus:</strong> {singleData.bus_detail}</p>
                                <p style={{ margin: '0.3rem' }}><strong>Bus Leader1:</strong> {singleData.bus_leader_1}</p>
                                <p style={{ margin: '0.3rem' }}><strong>Bus Leader1 no:</strong> {singleData.bus_leader_1_no}</p>
                                <p style={{ margin: '0.3rem' }}><strong>Bus Leader2:</strong> {singleData.bus_leader_2}</p>
                                <p style={{ margin: '0.3rem' }}><strong>Bus Leader2 no:</strong> {singleData.bus_leader_2_no}</p>
                                <p style={{ margin: '0.3rem' }}><strong>Nashik Utara:</strong> {singleData.nasik_utara}</p>
                                <p style={{ margin: '0.3rem' }}><strong>Pune Utara:</strong> {singleData.pune_utara}</p>
                                <p style={{ margin: '0.3rem' }}><strong>Tithal Utara:</strong> {singleData.tithal_utara}</p>
                                <p style={{ margin: '0.3rem' }}><strong>Quiz Marks: </strong> {singleData.total_marks}/{singleData.out_of}</p>
                                {singleData.shibir_session_1 && <p style={{ margin: '0.3rem' }}><strong>Session 1: </strong>  {singleData.shibir_session_1}</p>}
                                {singleData.shibir_session_1 && <p style={{ margin: '0.3rem' }}><strong>Session 2: </strong>  {singleData.shibir_session_2}</p>}
                                {singleData.shibir_session_1 && <p style={{ margin: '0.3rem' }}><strong>Session 3: </strong>  {singleData.shibir_session_3}</p>}
                                {singleData.shibir_session_1 && <p style={{ margin: '0.3rem' }}><strong>Session 4: </strong>  {singleData.shibir_session_4}</p>}
                                {singleData.shibir_session_1 && <p style={{ margin: '0.3rem' }}><strong>Session 5: </strong>  {singleData.shibir_session_5}</p>}
                                {singleData.shibir_session_1 && <p style={{ margin: '0.3rem' }}><strong>Session 6: </strong>  {singleData.shibir_session_6}</p>}

                            </DetailedViewData>
                        )}
                    </DetailedViewModal>
                </>
            )}
        </Background>
    )
}

const Background = styled.div`
    background: #e2c2ff;
    min-height: 100vh;
    display: flex;
    flex-direction: column;
`;

const TransparentBackground = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5);
`;

const Details = styled.button`
    display: flex;
    padding: 8px 10px;
    justify-content: center;
    align-items: flex-start;
    gap: 8px;
    flex: 1 0 0;
    border-radius: 8px;
    background: var(--again-new-gradient, linear-gradient(180deg, #C86FFF 0%, #BFA1FF 100%));
    color: var(--BG-Purple, #1D0F2A);
    font-family: arial;
    font-size: 16px;
    font-style: normal;
    font-weight: 600;
    line-height: 140%;
    letter-spacing: -0.165px;
    margin-left: 15px;
`;

const DetailedViewModal = styled.div`
    position: fixed;
    top: 50%;
    left: 50%;
    width: 90vw;
    height: 80vh;
    transform: translate(-50%, -50%);
    background: var(--Light-Pink-Gradient, linear-gradient(168deg, #FFF 0%, #E2C2FF 70.31%));
    padding: 20px;
    border-radius: 8px;
    box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
    z-index: 1000;
`;

const CloseButton = styled.button`
    position: absolute;
    top: 10px;
    right: 10px;
    border: none;
    border-radius: 100px;
    color: red;
    font-size: 16px;
    cursor: pointer;
`;

const DetailedViewData = styled.p`
    color: #333333;
    font-size: 16px;
`;

const BackIcon = styled.img`
    width: 13px;
    aspect-ratio: 0.65;
    object-fit: auto;
    fill: var(--BG-Gredient, linear-gradient(180deg, #270025 0%, #1d0f2a 100%));
    cursor: pointer;
    margin: 1.5rem;
`;

export default AdminDetails;
