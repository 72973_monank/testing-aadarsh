import React from 'react';
import styled from 'styled-components';
import { BACKEND_ENDPOINT } from '../api/api';
import { useNavigate } from 'react-router-dom';


const NityaVanchan = () => {
    const navigate = useNavigate();
    return (
        <Background>
            <Heading>Nitya Vanchan</Heading>
            <Container>
                <Button onClick={() => navigate('/vachnamrut')}>Vachnamrut</Button>
                <Button onClick={() => navigate('/swami-ni-vato')}>Swami ni vato</Button>
                <Button onClick={() => navigate('/video-prasang')}>Guruhari Prasangam</Button>
            </Container>
        </Background>
    )
}

const Background = styled.div`
    background: var(--Light-Pink-Gradient, linear-gradient(168deg, #FFF 0%, #E2C2FF 70.31%));
    height: 40vh;
    margin: 0.5rem;
    margin-bottom: 5rem;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    border-radius: 10px;
`;

const Heading = styled.h1`
    font-weight: bold;
    margin-bottom: 2rem;
`;

const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    align-items: center;
    gap: 1rem;
`;

const Button = styled.button`
    padding: 0.5rem 1rem;
    font-size: 1rem;
    border: none;
    width: 10rem;
    border-radius: 5px;
    background-color: #C86FFF;
    color: white;
    cursor: pointer;
    transition: background-color 0.3s ease;

    &:hover {
        background-color: #BFA1FF;
    }
`;

export default NityaVanchan;
