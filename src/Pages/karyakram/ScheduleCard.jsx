import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { BACKEND_ENDPOINT } from '../../api/api';

const ScheduleCard = () => {
  const navigate = useNavigate();
  const [karyakram, setKaryakram] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`${BACKEND_ENDPOINT}/karyakram/get_karyakram`);
        setKaryakram(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  }, []);

  return (
    <CardContainer>
      <BackIcon
        onClick={() => navigate(-1)}
        src="https://cdn.builder.io/api/v1/image/assets/TEMP/a9737027d53672d51e861c036db609e65e7478afbce397041e33ffa50b82a036?apiKey=3250d16d0ad044539de68d3e33600ce8&"
        alt="Back icon"
      />

      <Title>{Array.isArray(karyakram) && karyakram[0]?.day}</Title>

      {Array.isArray(karyakram) && karyakram.map((item) => (
        <TableCard key={item.id}>
          <DateTime>
            {item.time} ({item.place})
          </DateTime>
          <KaryakramText>
            {item.karyakram}
          </KaryakramText>
        </TableCard>
      ))}
    </CardContainer>
  );
};

const CardContainer = styled.div`
  background: linear-gradient(180deg, #fff 0%, #e2c2ff 100%);
  border-radius: 10px;
  padding: 20px;
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
  width: 100vw;
  max-width: 100%;
  min-height: 100vh;
  height: 100%;
  margin: 0 auto;
  margin-bottom: 3rem;
`;

const BackIcon = styled.img`
  width: 13px;
  aspect-ratio: 0.65;
  object-fit: auto;
  cursor: pointer;
`;

const Title = styled.h1`
  width: 100%;
  font-weight: bold;
  text-align: center;
  margin-bottom: 20px;
`;

const TableCard = styled.div`
  background: #ffffff;
  border-radius: 8px;
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
  margin-bottom: 10px;
  padding: 15px;
  display: flex;
  flex-direction: column;
`;

const DateTime = styled.div`
  font-weight: bold;
  color: #333;
  margin-bottom: 5px;
`;

const KaryakramText = styled.div`
  color: #666;
`;

export default ScheduleCard;
