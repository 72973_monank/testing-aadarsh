import * as React from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";

function Dhule() {
  const navigate = useNavigate();
  const headingStyle = {
    fontWeight: "bold",
    fontSize: "1.25rem"
  }
  return (
    <>
      <BackIcon onClick={() => navigate(-1)}>
        {/* Navigate back */}
        <img
          src="https://cdn.builder.io/api/v1/image/assets/TEMP/1fd569ca716b29d7f1a98071e37fd2287542114cc8243ecad11b40ab9ef936c8?apiKey=65b9bef5a9974c109a4afdb193963080&"
          alt="Back button"
        />
      </BackIcon>
      <MainContainer>
        <TemplePageContainer>
          <Header>
            <Title>BAPS Shree Swaminarayan Mandir, Dhule</Title>
          </Header>
          <TempleImageWrapper>
            <TempleImage
              src="https://www.baps.org//Data/Sites/1/Media/LocationImages/310BAPS_Shri_Swaminarayan_Mandir_Dhule_Mandir_moods_03.jpg"
              alt="BAPS Shree Swaminarayan Mandir, Dhule"
            />
          </TempleImageWrapper>
        </TemplePageContainer>

        <Content>
          <Paragraph style={headingStyle}>
            ધુલિયા
            <br />
          </Paragraph>
          <Paragraph>
            જયાં હરિમંદિરની શક્યતા ન હતી ત્યાં સન. ૧૯૭૬માં આર્ષદ્રષ્ટા પ્રમુખસ્વામી મહારાજે શિખરબદ્ધ મંદિર કરવાનો સંકલ્પ કરેલો.
            હૃદયની બીમારી હતી, ડોકટરોની સખત ના હતી, છતાં સ્વામીશ્રી ધૂલિયા પધાર્યા. અને તા. ૧૯-૬-૧૯૯૮ શુક્રવારે સં.૨૦૫૫ જેઠ વદ ૧૦ના રોજ હરિમંદિરની પ્રતિષ્ઠા અને છાત્રાલયના ઉદ્ઘાટનનો વિધિ કર્યો.
            ગુજરાત અને મહારાષ્ટ્રના વિધાર્થીઓ અહીં ધૂલિયા ભણવા આવતા. તેથી સ્વામીશ્રીએ પ્રથમ છાત્રાલય તૈયાર કરાવ્યું. પાસે જમીન ઉપર એક ખૂણો પડતો હતો. સ્વામીશ્રી કહે, ‘આ બાજુની જમીન પણ લ્યો.’ આનંદજીવન સ્વામી કહે, ‘ખૂબ જમીન છે. કયાં સાચવવી?’ સ્વામીશ્રી કહે, 'ના, ખાંચો ના જોઈએ, કામ આવશે, ખરીદી લેજો' (તેમને મન શિખરબદ્ધ મંદિરનો પ્લાન હતો.)
            તે એક એકર જમીન ખરીદાઈ ગઈ પછી સ્વામીશ્રી કહે, ‘ત્યાં કોઈ ઝાડ ન વાવશો!! આજુ બાજુના પ્લોટ પણ લઈ લ્યો.’ ફુલ-ઝાડના આગ્રહીને મન શિખરબદ્ધ મંદિરનો પ્લાન નક્કી હતો. તેથી ઝાડ ન વવરાવ્યા અને જમીન લેવાડાવી.
            તા. ૩-૭-૨૦૦૭ ના રોજ મહંતસ્વામી મહારાજે હરિમંદિરમાં વાઘાવાળા (પોષાક પહેરાવી શકાય તેવા) અક્ષર-પુરુષોત્તમ મહારાજની પ્રાણપ્રતિષ્ઠા કરી.

            <br />
          </Paragraph>
          <Paragraph>
            તા-૩-૭-૨૦૦૭ ના રોજ મહંતસ્વામી મહારાજે હરિમંદિરમાં વાધાવાળા (પોષાક પહેરાવી શકાય તેવા) અક્ષર પુરુષોત્તમ મહારાજની પ્રાણપ્રતિષ્ઠા કરી.

            <br />
          </Paragraph>
          <Paragraph style={headingStyle}>
            ધુલિયા શિખરબદ્ધ મંદિર
            <br />
          </Paragraph>
          <Paragraph>
            તા. ૪-૩-૨૦૧૩ ના રોજ અમદાવાદ ખાતે ધુલિયાના શિખરબદ્ધ મંદિરનું શિલાપૂજન પ.પૂ.પ્રમુખસ્વામી મહારાજે કર્યું.
            તા. ૧૦-૧૦-૨૦૧૩ ના રોજ ભૂમિપૂજન અને તા. ૨૫-૧૧-૨૦૧૩ ના રોજ શિલાન્યાસ વિધિ પ.પૂ.મહંતસ્વામી મહારાજે કર્યો.
            તા.૨૪-૭-૨૦૧૪ ના રોજ સારંગપુરમાં પ્રમુખસ્વામી મહારાજે સ્તંભપૂજન કર્યું.
            તા. ૬-૮-૨૦૧૫ ના રોજ સારંગપુરમાં પ.પૂ.પ્રમુખસ્વામી મહારાજે ધૂલિયાના શિખરબદ્ધ મંદિરમાં બિરાજમાન થનાર દરેક મૂર્તિઓની પ્રાણપ્રતિષ્ઠા કરી.
            ૧૨ એકર ભૂમિ ઉપર બંસીપહાડપુરના ગુલાબી પથ્થરોથી નિર્માણ આ સુંદર મંદિર પૂર્વમુખે છે. ૨૪ પગથિયાં છે, ઊંડી-જીણી-નાજુક કોતરણીથી આખુ મંદિર ભરચક છે, સુંદર અલૌકિક મંદિર મનને સ્થિર કરે છે. અહીં આવનાર વ્યક્તિ વારંવાર આવે છે અને સંસ્કાર અને શાંતી લઈને જાય છે.


            <br />
          </Paragraph>

          <Paragraph style={headingStyle}>
            અભિષેક મંડપ

            <br />
          </Paragraph>

          <Paragraph>
            મંદિર નીચે અભિષેક મંડપમાં આઠ આઠ સ્વર્ણિમ સિહાસનમાં ગુણાતીત ગુરુઓની મૂર્તિઓ તથા શ્રી વિઠ્ઠલજી-રુકિમણીજી, શ્રી લક્ષ્મીનારાયણ, સીતા-રામ, શિવ-પાર્વતીની મુર્તિઓ છે. મંડપ વચ્ચે શ્રી નીલકંઠવર્ણીની અભિષેક મુર્તિ છે. દર્શનાર્થીઓની મનોકામના પુર્ણ કરે છે. ભદ્રેશદાસ સ્વામીએ અક્ષરપુરુષોત્તમ દર્શનને પુરસ્કૃત કરતી નૂતન પ્રાણપ્રતિષ્ઠા વિધિની રચના કરી છે, તે મુજબનો સર્વ પ્રથમ વિધિ અહીં થયો.
            તા. ૮-૨-૧૯ ના રોજ નગરયાત્રા યોજાઈ. પાંચ કિલો મીટર લાંબો પંથ હતો. દોઢ કિ.મી. લાંબી શોભાયાત્રા હતી. તા. ૯-૨-૧૯ના રોજ વિશ્વશાંતિ મહાયાગ થયો, પ્રતિષ્ઠિત થનારી ૨૨મૂર્તિઓ ઉપર વેદોક્ત વિધિ થઈ, તા. ૧૦-૨-૧૯ વસંતપંચમીના શુભ દિવસે પ્રગટ બ્રહ્મસ્વરૂપ મહંતસ્વામી મહારાજના વરદ હસ્તે મૂર્તિપ્રતિષ્ઠા સંપન્ન થઈ.
            પ્રગટ બ્રહ્મસ્વરૂપ મહંતસ્વામી મહારાજે આશીર્વચનમાં કહ્યું કે “આ ભૂમિ મહાતીર્થ બની ગઈ છે. ભવિષ્યમાં આનો મહિમા ખૂબ વધશે."

            <br />
          </Paragraph>

          <Paragraph style={headingStyle}>
            પાંજરા નદી

            <br />
          </Paragraph>

          <Paragraph>
            નીલકંઠવર્ણી વેશે ભગવાન સ્વામિનારાયણે અહીં સ્નાન કર્યું છે, ભોજન કર્યું છે. પ.પૂ. મંહતસ્વામી મહારાજે કહ્યું “આ ધુલિયા અને પાંજરા નદી મહા પ્રસાદીની છે. નીલકંઠવર્ણી પધાર્યા છે. પાંજરા નદીમાં સ્નાન કર્યું છે, તેનો મહિમા ખૂબ છે "

            <br />
          </Paragraph>





        </Content>
      </MainContainer>
    </>
  );
}

const MainContainer = styled.div`
  position: relative;
  margin-top: 21px;
  height: auto;
  padding: 0 20px;
`;

const Header = styled.header`
  background-color: #fff;
`;

const Title = styled.h2`
  background-color: #fff;
`;

const Content = styled.section`
  background-color: #fff;
`;

const Paragraph = styled.p`
  background-color: #fff;
`;

const TemplePageContainer = styled.main`
  display: flex;
  flex-direction: column;
  max-width: 480px;
  width: 100%;
  margin: 0 auto;
`;
const BackIcon = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 1.5rem;
  margin-left: 1.5rem;
`;
const TempleTitle = styled.h1`
  color: var(--BG-Pink, #270025);
  font: 700 19px Overlock, sans-serif;
`;

const TempleImageWrapper = styled.figure`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-top: 17px;
  border-radius: 8px;
`;

const TempleImage = styled.img`
  width: 100%;
  aspect-ratio: 1.47;
  object-fit: cover;
  object-position: center;
  border-radius: 10px;
`;

export default Dhule;
