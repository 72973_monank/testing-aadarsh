import React from "react";
import styled from "styled-components";
import useLeaderboard from "../utils/useLeaderBoard";

const Leaderboard = ({ url }) => {
  const { data, loading, error } = useLeaderboard(url);

  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error: {error.message}</div>;

  return (
    <LeaderboardContainer>
      <LeaderboardTitle>Score board</LeaderboardTitle>
      <LeaderboardList>
        {data.map((item) => (
          <LeaderboardItem key={item.shibir_id}>
            <Name>{item.name}</Name>
            <Mandal>{item.mandal}</Mandal>
            <Details>
              <Marks>{item.total_marks}</Marks>
            </Details>
          </LeaderboardItem>
        ))}
      </LeaderboardList>
    </LeaderboardContainer>
  );
};

const LeaderboardContainer = styled.div`
  width: 100%;
  padding: 1rem;
  background: linear-gradient(0deg, #ffffff 0%, #e2c2ff 100%);

  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
  margin-bottom: 2rem;
`;

const LeaderboardTitle = styled.h2`
  margin-bottom: 1rem;
  text-align: center;
  font-size: 24px;
  font-weight: bold;
`;

const LeaderboardList = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
`;

const LeaderboardItem = styled.li`
  display: flex;
  justify-content: space-between;
  padding: 0.5rem 0;
  border-bottom: 1px solid #ddd;

  &:last-child {
    border-bottom: none;
  }
`;

const Name = styled.span`
  font-weight: bold;
`;

const Details = styled.span`
  display: flex;
  gap: 1rem;
`;

const Marks = styled.span`
  font-weight: bold;
`;

const Mandal = styled.span`
  color: #666;
`;

export default Leaderboard;
