import React from "react";
import styled from "styled-components";
import image1 from './../resources/memories_images/image1.jpg'
import image2 from './../resources/memories_images/image2.jpg'
import image3 from './../resources/memories_images/image3.jpg'
import image4 from './../resources/memories_images/image4.jpg'
import image5 from './../resources/memories_images/image5.jpg'
import image6 from './../resources/memories_images/image6.jpg'
import image7 from './../resources/memories_images/image7.jpg'
import image8 from './../resources/memories_images/image8.jpg'
import image9 from './../resources/memories_images/image9.jpg'
import image10 from './../resources/memories_images/image10.jpg'
import image11 from './../resources/memories_images/image11.jpg'
import image12 from './../resources/memories_images/image12.jpg'
import yuvati1 from './../resources/yuvati/yuvati1.jpg'
import yuvati2 from './../resources/yuvati/yuvati2.jpg'
import yuvati3 from './../resources/yuvati/yuvati3.jpg'
import yuvati4 from './../resources/yuvati/yuvati4.jpg'
import yuvati5 from './../resources/yuvati/yuvati5.jpg'



const yuvakImages = [
  {
    src: image1,
    aspectRatio: 1,
  },
  {
    src: image2,
    aspectRatio: 1.41,
  },
  {
    src: image3,
    aspectRatio: 1.41,
  },
  {
    src: image4,
    aspectRatio: 1,
  },
  {
    src: image5,
    aspectRatio: 1.52,
  },
  {
    src: image6,
    aspectRatio: 0.58,
  },
  {
    src: image7,
    aspectRatio: 1.41,
  },
  {
    src: image8,
    aspectRatio: 1.52,
  },
  {
    src: image9,
    aspectRatio: 0.58,
  },
  {
    src: image10,
    aspectRatio: 0.58,
  }
  ,
  {
    src: image11,
    aspectRatio: 0.58,
  }
  ,
  {
    src: image12,
    aspectRatio: 0.58,
  }



];

const yuvatiImages = [
  {
    src: image1,
    aspectRatio: 1,
  },
  {
    src: image2,
    aspectRatio: 1.41,
  },
  {
    src: image3,
    aspectRatio: 1.41,
  },
  {
    src: image4,
    aspectRatio: 1,
  },
  {
    src: image5,
    aspectRatio: 1.52,
  },
  {
    src: image6,
    aspectRatio: 0.58,
  },
  {
    src: image7,
    aspectRatio: 1.41,
  },
  {
    src: image8,
    aspectRatio: 1.52,
  },
  {
    src: yuvati1,
    aspectRatio: 0.58,
  },
  {
    src: image9,
    aspectRatio: 0.58,
  },
  {
    src: image10,
    aspectRatio: 0.58,
  }
  ,
  {
    src: image11,
    aspectRatio: 0.58,
  }
  ,
  {
    src: image12,
    aspectRatio: 0.58,
  },

  {
    src: yuvati2,
    aspectRatio: 0.58,
  },
  {
    src: yuvati3,
    aspectRatio: 0.58,
  },
  {
    src: yuvati4,
    aspectRatio: 0.58,
  },
  {
    src: yuvati5,
    aspectRatio: 0.58,
  },


];

const access = JSON.parse(localStorage.getItem("user"));


function MemoriesSection() {
  return (
    <MemoriesContainer>
      <MemoriesHeader>
        <MemoriesIcon
          src="https://cdn.builder.io/api/v1/image/assets/TEMP/d2b9d9559c9de94c411bb3c8d10fa32c60dc4c0d8a86a04165c45b39c474e9aa?apiKey=3250d16d0ad044539de68d3e33600ce8&"
          alt="Memories Icon"
        />
        <MemoriesTitle>Memories</MemoriesTitle>
        {access?.gender === "male" && <MemoriesLink href="https://www.dropbox.com/scl/fo/n00tzyg9h8c1iufr2tilr/AMUI4eyZTQObYYN5HHFbGhg/yuvak?rlkey=5x3lf8m9co8066rfkd5jezi2j&subfolder_nav_tracking=1&st=l5wc2isn&dl=0">View All</MemoriesLink>}
        {access?.gender === "female" && <MemoriesLink href="https://www.dropbox.com/scl/fo/n00tzyg9h8c1iufr2tilr/AOcRtc50SkSF-CuiWbiW0So/yuvti?rlkey=5x3lf8m9co8066rfkd5jezi2j&subfolder_nav_tracking=1&st=fw5a8m24&dl=0">View All</MemoriesLink>}
      </MemoriesHeader>
      <MemoriesGallery>
        {access?.gender === "male" && yuvakImages.map((image, index) => (
          <ImageWrapper key={index}>
            <Image
              src={image.src}
              alt={`Memory ${index + 1}`}
              aspectRatio={image.aspectRatio}
            />
          </ImageWrapper>
        ))}

        {access?.gender === "female" && yuvatiImages.map((image, index) => (
          <ImageWrapper key={index}>
            <Image
              src={image.src}
              alt={`Memory ${index + 1}`}
              aspectRatio={image.aspectRatio}
            />
          </ImageWrapper>
        ))}

      </MemoriesGallery>
    </MemoriesContainer>
  );
}

const MemoriesContainer = styled.section`
  display: flex;
  max-width: 100%;
  flex-direction: column;
`;

const MemoriesHeader = styled.header`
  display: flex;
  align-items: center;
  gap: 8px;
  margin: 8px 10px 0 10px;
  color: #1d0f2a;
  padding: 0 8px;
`;

const MemoriesIcon = styled.img`
  width: 28px;
  object-fit: contain;
  height: 28px;
`;

const MemoriesTitle = styled.h2`
  flex: 1;
  margin: auto 0;
  font: 700 20px Arial, sans-serif;
`;

const MemoriesLink = styled.a`
  font-size: 15px;
  font-weight: 400;
  text-decoration: none;
  color: black; // Default link color
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
`;

const MemoriesGallery = styled.div`
  display: flex;
  gap: 8px;
  margin-top: 10px;
  padding: 0 20px;
  overflow-x: auto;
  border-radius: 15px;
`;

const ImageWrapper = styled.div`
  flex: 0 0 auto; // This ensures that the wrapper does not grow or shrink.
  width: calc(175px * ${(props) => props.aspectRatio});
  height: 175px;
`;
// const ImageWrapper = styled.div`
//   display: flex;
//   flex-direction: column;
//   align-items: center;
// `;
// const Image = styled.img`
//   width: 175px;
//   object-fit: cover;
//   aspect-ratio: ${(props) => props.aspectRatio};

//   &:not(:first-child) {
//     margin-top: 8px;
//   }
const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export default MemoriesSection;
