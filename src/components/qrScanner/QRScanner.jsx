import React, { useEffect, useRef, useState, useCallback } from 'react';
import { Html5Qrcode } from 'html5-qrcode';
import axios from 'axios';
import { useAuth } from '../../contexts/AuthProvider';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import styled from 'styled-components';
import { BACKEND_ENDPOINT } from '../../api/api';

const QRScanner = ({ handleScan, handleError, handleClose }) => {
  const [isScannerOpen, setIsScannerOpen] = useState(false);
  const [isProcessing, setIsProcessing] = useState(false);
  const lastScannedCode = useRef(new Map());
  const { user: authUser } = useAuth();
  const scanInterval = 4000; // 2 seconds interval to prevent rapid scanning
  const scannerRef = useRef(null);
  const isScannerRunning = useRef(false);

  const onScanSuccess = useCallback(
    async (decodedText) => {
      if (decodedText && !isProcessing) {
        const code_data = decodedText.split(' ');
        console.log(code_data);
        const code = code_data[0];
        const now = Date.now();

        if (
          !lastScannedCode.current.has(code) ||
          now - lastScannedCode.current.get(code) > scanInterval
        ) {
          setIsProcessing(true);
          lastScannedCode.current.set(code, now);

          try {
            const response = await axios.post(
              `${BACKEND_ENDPOINT}/attendance/post_attendance`,
              { shibir_id: authUser?.id, yuvak_id: code }
            );
            console.log(response.data.message, "qr scan message");

            if (response.data.message === "Thank you for attendance") {
              toast.success('Attendance marked successfully!', {
                theme: 'colored',
                draggablePercent: 60,
              });
            } else if (response.data.message === "Attendance already done") {
              toast.warn('Attendance already done!', {
                theme: 'colored',
                draggablePercent: 60,
              });
            }
            else {
              toast.error(response.data.message || 'Failed to mark attendance!', {
                theme: 'colored',
                draggablePercent: 60,
              });
            }
          } catch (err) {
            console.error('Error posting attendance:', err);
            handleError(err);
          } finally {
            setIsProcessing(false);
            setTimeout(() => {
              lastScannedCode.current.set(code, now - scanInterval - 1);
            }, scanInterval);
          }
        }
      }
    },
    [isProcessing, authUser?.id, handleError, scanInterval]
  );

  useEffect(() => {
    if (isScannerOpen) {
      const scanner = new Html5Qrcode("reader");
      isScannerRunning.current = true;
      scanner.start(
        { facingMode: "environment" },
        { fps: 10, qrbox: 250 },
        onScanSuccess,
        handleError
      ).catch((err) => {
        console.error('Error starting scanner:', err);
        handleError(err);
        isScannerRunning.current = false;
      });

      scannerRef.current = scanner;
    }

    return () => {
      if (scannerRef.current && isScannerRunning.current) {
        scannerRef.current.stop().catch((err) => {
          console.error('Error stopping scanner:', err);
        }).finally(() => {
          isScannerRunning.current = false;
        });
        scannerRef.current = null;
      }
    };
  }, [isScannerOpen, onScanSuccess, handleError]);

  const handleOpenScanner = () => {
    if (!isProcessing && !isScannerRunning.current) {
      setIsScannerOpen(true);
    } else {
      toast.warning('Scanner is not ready yet. Please wait a moment.', {
        theme: 'colored',
        draggablePercent: 60,
      });
    }
  };

  const handleCloseScanner = () => {
    setIsScannerOpen(false);
    setIsProcessing(false);
    if (scannerRef.current && isScannerRunning.current) {
      scannerRef.current.stop().catch((err) => {
        console.error('Error stopping scanner:', err);
      }).finally(() => {
        isScannerRunning.current = false;
      });
      scannerRef.current = null;
    }
    lastScannedCode.current.clear();
    handleClose();
  };

  return (
    <div style={{ width: '100%' }}>
      {isScannerOpen ? (
        <div>
          <StyledButton onClick={handleCloseScanner}>Close Scanner</StyledButton>
          <div id="reader" style={{ width: '100%' }}></div>
        </div>
      ) : (
        <StyledButton onClick={handleOpenScanner}>Open Scanner</StyledButton>
      )}
      <ToastContainer position="top-center" autoClose={2000} />
    </div>
  );
};

const StyledButton = styled.button`
  margin-bottom: 10px;
  border-radius: 8px;
  border: 0.5px solid rgba(29, 15, 42, 0.50);
  background: linear-gradient(180deg, #270025 0%, #1D0F2A 100%);
  color: white;
  padding: 10px 20px;
  cursor: pointer;

  &:hover {
    background: linear-gradient(180deg, #1D0F2A 0%, #270025 100%);
  }
`;

export default QRScanner;
