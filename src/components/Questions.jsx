import React, { useState, useEffect, useCallback } from "react";
import axios from "axios";
import "../utils/QuizComponent.css";
import { useAuth } from "../contexts/AuthProvider";
import { ToastContainer, toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import styled from "styled-components";
import { BACKEND_ENDPOINT } from "../api/api";

const QuizComponent = () => {
  const [questions, setQuestions] = useState([]);
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const [selectedOption, setSelectedOption] = useState("");
  const [noData, setNoData] = useState(false);
  const [timer, setTimer] = useState(120); // Initial timer per question
  const [totalTime, setTotalTime] = useState(0); // Total quiz time in seconds
  const [elapsedTime, setElapsedTime] = useState(0); // Elapsed time in seconds
  const [answers, setAnswers] = useState([]);
  const { user: authUser } = useAuth();
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    // Fetch questions from the API
    const fetchQuestions = async () => {
      try {
        const response = await axios.post(
          `${BACKEND_ENDPOINT}/quiz/get_questions`,
          {
            shibir_id: authUser?.id,
          }
        );
        console.log(response.data, "response.data-----");
        if (!response.data.status) {
          setNoData(true);
          return;
        }
        setQuestions(response.data.questions);
        setTotalTime(response.data.questions.length * 120); // Total time based on number of questions
      } catch (error) {
        console.error("Error fetching questions:", error);
      }
    };
    fetchQuestions();
  }, [authUser]);

  useEffect(() => {
    const countdown = setInterval(() => {
      setTimer((prevTimer) => {
        if (prevTimer <= 0) {
          handleNextQuestion();
          return 120; // Reset timer to 120 seconds per question
        }
        return prevTimer - 1;
      });

      setElapsedTime((prevElapsedTime) => prevElapsedTime + 1);
    }, 1000);

    if (elapsedTime >= totalTime) {
      clearInterval(countdown);
    }

    return () => clearInterval(countdown);
  }, [elapsedTime, totalTime]);

  const handleOptionChange = (event) => {
    setSelectedOption(event.target.value);
  };

  const handleNextQuestion = useCallback(() => {
    if (selectedOption) {
      setAnswers((prevAnswers) => [
        ...prevAnswers,
        {
          shibir_id: authUser?.id,
          quiz: questions[0]?.quiz,
          id: questions[currentQuestionIndex].id,
          selected_option: selectedOption,
        },
      ]);
    }
    if (currentQuestionIndex < questions.length - 1) {
      setCurrentQuestionIndex(currentQuestionIndex + 1);
      setSelectedOption("");
      setTimer(120); // Reset timer to 120 seconds
    } else {
      handleSubmitAnswers();
    }
  }, [currentQuestionIndex, questions, selectedOption, authUser]);

  const handleSubmitAnswers = async () => {
    setLoading(true); // Set loading to true when the submit button is pressed
    try {
      await axios.post(`${BACKEND_ENDPOINT}/quiz/post_answers`, answers);
      toast.success("Answers submitted successfully!");
      navigate("/activity-page");
    } catch (error) {
      console.error("Error submitting answers:", error);
    } finally {
      setLoading(false); // Set loading to false once the request is complete
    }
  };

  const formatTime = (time) => {
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;
    return `${minutes}:${seconds < 10 ? "0" : ""}${seconds}`;
  };

  const formatTotalTime = (time) => {
    const hours = Math.floor(time / 3600);
    const minutes = Math.floor((time % 3600) / 60);
    return `${hours > 0 ? `${hours}h ` : ""}${minutes}m`;
  };

  if (noData) {
    return (
      <NoDataContainer>
        <h2>You have already given the quiz.</h2>
      </NoDataContainer>
    );
  } else if (questions.length === 0) {
    return <div>Loading...</div>;
  }

  const currentQuestion = questions[currentQuestionIndex];

  return (
    <div className="quiz-container">
      <h2 className="quiz-question">{currentQuestion?.question}</h2>
      <div className="quiz-radio-group">
        {[1, 2, 3, 4].map(
          (num) =>
            currentQuestion[`option_${num}`] && (
              <label
                key={num}
                className={`quiz-radio-label ${
                  selectedOption === num.toString() ? "selected" : ""
                }`}
              >
                <input
                  type="radio"
                  value={num}
                  checked={selectedOption === num.toString()}
                  onChange={handleOptionChange}
                  className="quiz-radio-input"
                />
                {currentQuestion[`option_${num}`]}
              </label>
            )
        )}
      </div>
      <div className="quiz-buttons">
        {currentQuestionIndex < questions.length - 1 ? (
          <button onClick={handleNextQuestion} className="quiz-button">
            Next
          </button>
        ) : (
          <button
            onClick={handleSubmitAnswers}
            className="quiz-button"
            disabled={loading} // Disable the button when loading
          >
            {loading ? "Submitting..." : "Submit Answers"}
          </button>
        )}
      </div>
      <div className="quiz-timer">
        <CircularProgressbar
          value={(timer / 120) * 100}
          text={`${formatTime(timer)}`}
          styles={buildStyles({
            pathColor: `rgba(62, 152, 199, ${timer / 120})`,
            textColor: "#000",
            trailColor: "#d6d6d6",
            textSize: 30,
            backgroundColor: "#f5f5f5",
          })}
        />
      </div>
      <ToastContainer position="top-center" autoClose={5000} />
    </div>
  );
};

export default QuizComponent;

const NoDataContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 60vh;
  font-size: 24px;
  color: #b200ff;
  font-family: cursive;
  font-weight: bold;
`;

const ScoreValue = styled.span`
  font-size: 14px;
  color: #1d0f2a;
  font-family: Sora, sans-serif;
  @media (min-width: 768px) {
    font-size: 23px;
  }
  @media (min-width: 1024px) {
    font-size: 40px;
  }
`;
